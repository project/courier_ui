<?php

namespace Drupal\courier_ui;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for training_level entity.
 */
class TranslatableMailTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
