<?php

namespace Drupal\courier_ui\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a template collection.
 */
class DeleteTemplateCollection extends ContentEntityDeleteForm {

}
